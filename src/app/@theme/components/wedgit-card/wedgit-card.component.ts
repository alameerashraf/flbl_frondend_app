import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wedgit-card',
  templateUrl: './wedgit-card.component.html',
  styleUrls: ['./wedgit-card.component.scss']
})
export class WedgitCardComponent implements OnInit {


  @Input() title: string;
  @Input() type: string;
  @Input() label: string;
  
  constructor() { }

  ngOnInit() {
  }

}
