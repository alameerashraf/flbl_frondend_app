import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { MAT_MODAL , NgxMask , NBULAR_THEME , NgbModules} from './admin.imports';


import * as adminComponents from './components';
import { ThemeModule } from '../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from '../user/user-routing.module';
import { httpService, localStorageService } from '../@core';
import { EditCountryComponent } from './components/edit-country/edit-country.component';
import { DeleteCountryComponent } from './components/delete-country/delete-country.component';

@NgModule({
  declarations: [
    adminComponents.CountryComponent,
    EditCountryComponent,
    DeleteCountryComponent
  ],
  imports: [
    MAT_MODAL,
    ThemeModule,
    NgxMask.forRoot(),
    ...NBULAR_THEME,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    UserRoutingModule,
    NgbModules,
    AdminRoutingModule
  ],
  providers: [
    httpService,
    localStorageService,
  ],
})
export class AdminModule { }
