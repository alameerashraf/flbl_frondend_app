import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { urls , httpService } from './../../../@core';
import { post } from './../../models/post';
import { localStorageService } from './../../../@core/services/local-storage/local-storage';

@Component({
  selector: 'ngx-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.scss']
})
export class EditCountryComponent implements OnInit {
  countryId: any;
  post: post;

  constructor(private ActivatedRoute: ActivatedRoute, private httpService: httpService , private localStorageService: localStorageService) { }

  ngOnInit() {
    this.ActivatedRoute.params.subscribe((params) => {
      this.countryId = params["countryID"];
      this.loadPost();
    });
  }


  loadPost(){
    let singlePostURL = `${urls.LOAD_OFFER}/${this.countryId}`;
    this.httpService.Get(singlePostURL).subscribe((response: post) => {
      this.post = response;
      this.localStorageService.setLocalStorage("post" , response)
    })
  }

  saveEditing(){

  }
}
