import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { localStorageService, httpService, urls, authorizationService, responseModel, constants } from "./@core";
import { userRolesModel } from './@core/services/auth/userRolesModel';
import { headersModel } from './@core/services/http/headersModel';

@Component({
  selector: "seqt-app-landing",
  template: "<p></p>"
})
export class AppLandingComponent implements OnInit {
  loggedInUser = {
    userName: "",
    userAvatar: "",
    userSESA: "",
    token: "",
    isManager: false,
    userRoles: []
  };
  returnUrl: any = "";

  constructor(
    private route: ActivatedRoute,
    private storageService: localStorageService,
    private httpService: httpService,
    private authorizationService: authorizationService
  ) {

  }

  ngOnInit() {}
}
