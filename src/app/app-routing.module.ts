import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import * as AppModuleComponents from './app-components';


const routes: Routes = [

  
  {
    path: '',
    component: AppModuleComponents.AppStarterComponent,
    children: [
      { 
        path: 'privacy',
        component: AppModuleComponents.PrivacyComponent
      },
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module')
          .then(m => m.AdminModule),
      }
    ]
  },
  { 
    path: 'login',
    component: AppModuleComponents.AppLandingComponent
  },
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: '**', redirectTo: 'pages' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
