import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";

import { NbAuthModule, NbDummyAuthStrategy } from "@nebular/auth";
import { throwIfAlreadyLoaded } from "./module-import-guard";

import { NB_CORE_PROVIDERS } from "./theme/providers";
import { localStorageService } from './services/local-storage/local-storage';
import { httpService } from '.';

@NgModule({
  imports: [
  CommonModule, 
    HttpClientModule,
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
  providers: [
    localStorageService,
    httpService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, "CoreModule");
  }

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [...NB_CORE_PROVIDERS]
    };
  }
}
