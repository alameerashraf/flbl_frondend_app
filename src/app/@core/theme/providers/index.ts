import { NbRoleProvider, NbSecurityModule } from "@nebular/security";
import { of as observableOf } from "rxjs";
import { NbAuthModule, NbDummyAuthStrategy } from "@nebular/auth";

import * as Mock from "../mock";
import * as Data from "../data";
import * as Util from "../utils";

const DATA_SERVICES = [
  { provide: Data.UserData, useClass: Mock.UserService },
  { provide: Data.ElectricityData, useClass: Mock.ElectricityService },
  { provide: Data.SmartTableData, useClass: Mock.SmartTableService },
  { provide: Data.UserActivityData, useClass: Mock.UserActivityService },
  { provide: Data.OrdersChartData, useClass: Mock.OrdersChartService },
  { provide: Data.ProfitChartData, useClass: Mock.ProfitChartService },
  { provide: Data.TrafficListData, useClass: Mock.TrafficListService },
  { provide: Data.EarningData, useClass: Mock.EarningService },
  {
    provide: Data.OrdersProfitChartData,
    useClass: Mock.OrdersProfitChartService
  },
  { provide: Data.TrafficBarData, useClass: Mock.TrafficBarService },
  {
    provide: Data.ProfitBarAnimationChartData,
    useClass: Mock.ProfitBarAnimationChartService
  },
  {
    provide: Data.TemperatureHumidityData,
    useClass: Mock.TemperatureHumidityService
  },
  { provide: Data.SolarData, useClass: Mock.SolarService },
  { provide: Data.TrafficChartData, useClass: Mock.TrafficChartService },
  { provide: Data.StatsBarData, useClass: Mock.StatsBarService },
  { provide: Data.CountryOrderData, useClass: Mock.CountryOrderService },
  {
    provide: Data.StatsProgressBarData,
    useClass: Mock.StatsProgressBarService
  },
  {
    provide: Data.VisitorsAnalyticsData,
    useClass: Mock.VisitorsAnalyticsService
  },
  { provide: Data.SecurityCamerasData, useClass: Mock.SecurityCamerasService }
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf("guest");
  }
}

export const NB_CORE_PROVIDERS = [
  ...Mock.MockDataModule.forRoot().providers,
  ...DATA_SERVICES,
  ...NbAuthModule.forRoot({
    strategies: [
      NbDummyAuthStrategy.setup({
        name: "email",
        delay: 3000
      })
    ]
  }).providers,

  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: "*"
      },
      user: {
        parent: "guest",
        create: "*",
        edit: "*",
        remove: "*"
      }
    }
  }).providers,

  {
    provide: NbRoleProvider,
    useClass: NbSimpleRoleProvider
  },
  Util.AnalyticsService,
  Util.LayoutService,
  Util.StateService
];
