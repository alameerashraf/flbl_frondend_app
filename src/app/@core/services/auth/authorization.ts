import { Injectable } from '@angular/core';
import { userRolesModel } from './userRolesModel';
import { localStorageService } from '../local-storage';
import { constants } from '../../helpers';

@Injectable()
export class authorizationService {
  roles: userRolesModel[] = [];

  constructor(){
    let userRoles = window.localStorage.getItem("loggedInUserRoles") as any;
    this.roles = JSON.parse(userRoles);
  }

  isSuperAdmin(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.SUPER_ADMIN);
    return isFound != undefined ? true : false;
  };

  isMarketingAdmin(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.MARKETING_ADMIN);
    return isFound != undefined ? true : false;
  };

  isFinancialAdmin(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.FINANCIAL_ADMIN);
    return isFound != undefined ? true : false;
  };

  isToolAdmin(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.TOOL_ADMIN);
    return isFound != undefined ? true : false;
  };

  isProjectManager(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.PROJECT_MANAGER);
    return isFound != undefined ? true : false;
  };

  isSalesManager(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.SALES_MANAGER);
    return isFound != undefined ? true : false;
  };

  isSalesEngineer(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.SALES_ENGINEER);
    return isFound != undefined ? true : false;
  };

  isTenderingManager(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.TENDEDRING_MANAGER);
    return isFound != undefined ? true : false;
  };

  isTenderingEngineer(){
    let isFound = this.roles.find(x => x.name == constants.ROLES.TENDERING_ENGINEER);
    return isFound != undefined ? true : false;
  };

  isHasAnOperationRole(){
    return (this.isTenderingEngineer() || this.isTenderingManager() || this.isSalesEngineer() || this.isSalesManager() || this.isProjectManager());
  }

}
