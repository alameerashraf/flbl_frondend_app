import { AppConfig } from '../../config';



export class urls{
    public static _config: AppConfig;
    public static get BASE_URL(): string { return  urls._config.getConfig("BASE_URL")};
    public static get SERVER_URL(): string { return  urls._config.getConfig("SERVER_URL")};
    public static get PING(): string { return urls._config.getConfig("SSO_PING") };

    //SSO
    public static get SSO(): string { return this.PING + '/Home/auth?returnUrl=' };

    // utilities 
    public static get UTILITY() :string { return '/env' };


    // internal routes 
    public static get USER_DASHBOARD_INTERNAL() :string { return 'users/dashboard' };


    // API Controllers..
    public static get LOAD_DATA_FROM_BFO(): string { return this.BASE_URL + '/bFO/load-opportunity-data' };

    public static get LOAD_OFFER(): string { return 'https://jsonplaceholder.typicode.com/posts' };

}