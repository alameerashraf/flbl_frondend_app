import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { urls , authorizationService, localStorageService, constants } from './@core';

@Component({
  selector: 'seqt-app-starter',
  template: `
    <seqt-one-column-layout>
      <nb-menu [items]="MENU_ITEMS"></nb-menu>
      <router-outlet></router-outlet>
    </seqt-one-column-layout>`,
})
export class AppStarterComponent implements OnInit {
  allMenuItems: any[] = [
    {
      title: 'Kick Starter',
      icon: 'activity-outline',
      link: '/admin/country',
      home: true,
      id : "STARTER_LINK"
    }
  ];
  MENU_ITEMS: NbMenuItem[] = [];

  constructor(private authorizationService: authorizationService , 
      private storageService: localStorageService) { }

  ngOnInit() {

    // Laod the menu items dynamically
    this.initMenuItems();

  }

  initMenuItems() {
    let allLinks = [];

    allLinks = allLinks.concat(this.allMenuItems.find(x => x.permission == "STARTER_LINK"))

    allLinks.forEach((linkItem) => {
      this.MENU_ITEMS.push(linkItem);
    })
  }
}


